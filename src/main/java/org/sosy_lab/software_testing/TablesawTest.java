package org.sosy_lab.software_testing;

import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;
import static org.assertj.core.api.Assertions.assertThat;

public class TablesawTest {
    public static void main(String[] args) throws Exception {
        Table data = Table.read().csv(
            CsvReadOptions.builder("tso-iso-rates.csv").missingValueIndicator(":").build());
        System.out.println(data.print(5));

	assertThat(data.rowCount())
	    .isGreaterThanOrEqualTo(100)
	    .isGreaterThanOrEqualTo(101); //Isequalt(102); //, "Too few rows. Should be 102, is " + data.rowCount());
    }
}
